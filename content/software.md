---
title: "Software that I use"
date: 2022-02-01T14:25:20+01:00
draft: false
---

tl;dr
{.no-pushdown}

- [Arch Linux](#arch-linux)
	- [Wayland](#wayland)
- [Firefox Nightly](#firefox-nightly)

# Arch Linux
Do you use MacOS or, god forbid, Microsoyft Windows?
I don't, because they are nonfree malware [^1].
Your criteria might differ, but I need an operating system that

1. Is free as in **freedom**, not beer (also called *libre*)
1. Is Unix-like
1. Is minimal, secure and respects my privacy
1. Runs on most hardware, even if it's more than a decade old
1. Has fast, sane packagement with more or less up to date software
{type="a"}

Most operating systems or distros fall flat after the 3rd requirement, the
remaining ones being FreeBSD, Alpine and Arch.
I tried using Alpine Linux for a while due to its use of OpenRC, but stuck with
[Arch Linux](https://archlinux.org) after running into some issues.

[^1]: See [here](https://www.gnu.org/proprietary/malware-microsoft.html) and
[here](https://www.gnu.org/proprietary/malware-apple.html)

## Wayland
If you haven't heard about Wayland, it's a new display protocol aiming to
replace our rusty friend X11, whose age has shown in the last years.
Believe it or not, Wayland is ready for desktop usage.
I currently use the leading compositor [Sway](https://swaywm.org).
Normally I'd use a dynamic tiler such as river or dwm, but the bspwm-like tiling
I set up is also quite enjoyable.

Other tools I use alongside of sway are
{.no-pushdown}
- [Swayidle](https://github.com/swaywm/swayidle) and
[Swaylock](https://github.com/swaywm/swaylock)
- [i3status-rust](https://github.com/greshake/i3status-rust) to fill my
statusbar
- [Kanshi](https://sr.ht/~emersion/kanshi/) for dynamic display configuration
- [Mako](https://wayland.emersion.fr/mako/) to display notifications
- [Fuzzel](https://codeberg.org/dnkl/fuzzel) as a menu and app launcher
- [Gammastep](https://gitlab.com/chinstrap/gammastep) to adjust the screen
temperature

Most, if not all of these tools show a degree of professionalism which puts
nonfree projects to shame.
They are extremely reliable and a real joy to use.

# Firefox Nightly
The browser: the most important software on any modern computer.
As embarassing as Mozilla is, they are still better than the competition.
If given the choice between Chrome/Edge, browsers designed to report your entire
browsing history back to Google/Microsoft, Brave, who supports crypto and
therefore ponzi schemes and environmental damage, and Firefox,
well the choice is clear.

Out of all the flavors Mozilla provides, I use
[Firefox Nightly](https://www.mozilla.org/firefox/channel/desktop/#nightly).
With recent movements of Firefox development going in the right direction and
the web being more fast-paced than ever, I'd like to be as rolling-release as
possible.
